## Opis

Otwórz *pull request* i dodaj nowy przepis na chleb na zakwasie, albo jakąś informację o tworzeniu i przechowywaniu zakwasu, rodzajach mąki, itp. Możesz też modyfikować przepisy istniejące.
